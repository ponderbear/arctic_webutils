# Set 'onbuild' arguments of the base image
# User and repository location
ARG USER=Ahab
ARG GROUP=Ahab
ARG UID=1000
ARG GID=1000
ARG REPO_DIR=/srv/repo_nginx

ARG BASE_IMG_VERSION=3.20.3-r1-onbuild
FROM icebear8/gitrepoutils:${BASE_IMG_VERSION}

# User access
ENV USER=${USER}
ENV GROUP=${GROUP}

RUN apk update && apk add --no-cache \
    nginx=1.26.2-r0

# Prepare helper tools and scripts for the application
# Setup the app utils directory and copy the prepared utilities to it
ENV APP_UTILS_DIR=/opt/scripts
ENV APP_LAUNCHER=${APP_UTILS_DIR}/launcher.sh
RUN mkdir -p ${APP_UTILS_DIR}
COPY /utils/* ${APP_UTILS_DIR}/

# Prepare nginx directory structure
# Nginx is configured to use these directories
# The configuration from the git repository is added with symlinks
ENV NGX_RESOURCE_DIR=/srv/nginx
ENV NGX_CONTENT_DIR=${NGX_RESOURCE_DIR}/www
ENV NGX_CONFIG_DIR=/etc/nginx/sites-enabled

# The repo directories with the content for nginx
# The repo directory is prepared with default data
# If a remote repository exists, the default data will be erased
# and overwritten with the repo content.
ENV NGX_CONFIG_REPO_DIR=${REPO_DIR}/nginx/config
ENV NGX_CONTENT_REPO_DIR=${REPO_DIR}/nginx/www

# Prepare directories
# Create symlinks from nginx dirs to the repository
RUN mkdir -p ${NGX_RESOURCE_DIR} && \
    mkdir -p ${NGX_CONFIG_REPO_DIR} && \
    ln -s ${NGX_CONFIG_REPO_DIR} ${NGX_CONFIG_DIR} && \
    mkdir -p ${NGX_CONTENT_REPO_DIR} && \
    ln -s ${NGX_CONTENT_REPO_DIR} ${NGX_CONTENT_DIR}

# Prepare nginx settings
# Copy initial default settings as well (not from git repo)
COPY ./resources/nginx.conf /etc/nginx/nginx.conf
COPY ./resources/config/default ${NGX_CONFIG_REPO_DIR}/
COPY ./resources/www/* ${NGX_CONTENT_REPO_DIR}/

# Remove nginx default content
# It is covered by `NGX_CONTENT_REPO_DIR`
RUN rm -rf /var/www
# Prepare pid file for nginx, to allow user access to it
RUN touch /var/run/nginx.pid

# Set access rights for the user to allow non-root nginx
RUN chmod -R 755 ${APP_UTILS_DIR}/*.sh
RUN chown -R ${USER}:${GROUP} ${REPO_DIR} && \
    chown -R ${USER}:${GROUP} ${NGX_RESOURCE_DIR} && \
    chown -R ${USER}:${GROUP} /var/log/nginx && \
    chown -R ${USER}:${GROUP} /var/lib/nginx && \
    chown -R ${USER}:${GROUP} /var/run/nginx.pid && \
    chown -Rh ${USER}:${GROUP} /var/lib/nginx

WORKDIR ${REPO_DIR}

VOLUME ${REPO_DIR}
VOLUME ${GIT_CONFIG_DIR}

USER ${USER}
EXPOSE 8080
EXPOSE 8443

ENTRYPOINT ["sh", "-c"]
CMD ["sh ${APP_LAUNCHER}"]
